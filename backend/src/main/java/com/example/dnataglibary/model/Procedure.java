package com.example.dnataglibary.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.time.LocalDate;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity(name = "test_procedure")
@Table(name = "test_procedure")
public class Procedure {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "id")
  private Long id;
  private LocalDate createdAt;
  private String editor;
  private String notes;
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "dna_sequence", referencedColumnName = "id")
  private DnaSequence dnaSequence;
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "generated_sequence", referencedColumnName = "id")
  private DnaSequence generatedSequence;
  private int scores;
}
