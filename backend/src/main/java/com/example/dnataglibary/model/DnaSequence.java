package com.example.dnataglibary.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "dna")
@Table(name = "dna")
@EqualsAndHashCode
public class DnaSequence {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "id")
  private Long id;
  private LocalDate createdAt;
  @Column(name = "dna_forward")
  private String forward;
  private double primerConc;
  private double saltConc;
  @Column(name = "dna_reverse")
  private String reverse;
  private String complementerTtoF;
  private String complementerFtoT;
  private int aContent, tContent, cContent, gContent;
  private double molecularWeight;
  private double gcContent;
  private int aatt, atta, taat, cagt, ctga, gact, gtca, cggc, gccg, ggcc;
  private double deltaG, deltaH, deltaS;
  private double rlnkValue;
  private double tmBasic;
  private double tmSalty;
  private double tmNn;
  private boolean isGenerated = false;
  private boolean selfdimer;
  private boolean hairloop;
}
