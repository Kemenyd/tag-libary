package com.example.dnataglibary.repository;

import com.example.dnataglibary.model.DnaSequence;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DnaTagRepository extends JpaRepository<DnaSequence, Long> {

  List<DnaSequence> findAllByIsGeneratedFalseOrderByCreatedAtDesc();
}
