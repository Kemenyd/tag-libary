package com.example.dnataglibary.mapper;

import com.example.dnataglibary.model.Procedure;
import com.example.dnataglibary.web.dto.ProcedureInputRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProcedureMapper {

  @Mapping(target = "generatedSequence", ignore = true)
  @Mapping(target = "dnaSequence", ignore = true)
  @Mapping(target = "id", ignore = true)
  @Mapping(target = "createdAt", ignore = true)
  @Mapping(target = "scores", ignore = true)
  Procedure toEntity(ProcedureInputRequest procedureInputRequest);
}
