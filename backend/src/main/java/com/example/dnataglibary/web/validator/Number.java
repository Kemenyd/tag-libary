package com.example.dnataglibary.web.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = SequenceValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Number {
    String message() default "Invalid nanomol number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
