package com.example.dnataglibary.web.dto;

import com.example.dnataglibary.web.validator.Number;
import com.example.dnataglibary.web.validator.Sequence;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class SequenceInputRequest {
  @NotBlank
  @Sequence
  private String forward;
  @Number
  @NotBlank
  private double primerConc;
  @Number
  @NotBlank
  private double saltConc;
}
