package com.example.dnataglibary.util;

public class CommonUtil {

    /**
     * Reversing the forward ssDNA string (last nucleotide -> first)
     *
     * @return String - containing the reverse ssDNA string (in 5'-3' direction)
     */
    public static String makeReverse(String sequence) {
        StringBuilder sb = new StringBuilder(sequence);
        return sb.reverse().toString();
    }

    /**
     * Making the complementer ssDNA string in 3'-5' direction
     *
     * @return String - containing the ssDNA string (in 3'-5' direction)
     */
    public static String makeTtoFComplementer(char[] forward) {
        StringBuilder sb = new StringBuilder();
        for (char c : forward) {
            switch (c) {
                case 'a':
                    sb.append('a');
                    break;
                case 'c':
                    sb.append('c');
                    break;
                case 'g':
                    sb.append('g');
                    break;
                default:
                    sb.append('t');
                    break;
            }
        }
        return sb.toString();
    }

    /**
     * Making the complementer ssDNA string in 5'-3' direction
     *
     * @return String - containing the ssDNA string (in 5'-3' direction)
     */
    public static String makeFtoTComplementer(String complementerTtoF) {
        StringBuilder sb = new StringBuilder(complementerTtoF);
        return sb.reverse().toString();
    }

    public static int aContent(String sequence) {
        var string = sequence.toLowerCase().toCharArray();
        int aContent = 0;
        for (char c : string) {
            if (c == 'a') {
                aContent++;
            }
        }
        return aContent;
    }

    public static int tContent(String sequence) {
        var string = sequence.toLowerCase().toCharArray();
        int tContent = 0;
        for (char c : string) {
            if (c == 't') {
                tContent++;
            }
        }
        return tContent;
    }

    public static int gContent(String sequence) {
        var string = sequence.toLowerCase().toCharArray();
        int gContent = 0;
        for (char c : string) {
            if (c == 'g') {
                gContent++;
            }
        }
        return gContent;
    }

    public static int cContent(String sequence) {
        var string = sequence.toLowerCase().toCharArray();
        int cContent = 0;
        for (char c : string) {
            if (c == 'c') {
                cContent++;
            }
        }
        return cContent;
    }

    /**
     * Calculating the molecular weight
     *
     * @return double - the molecular weight of the sequence
     */
    public static double mwCalc(int aContent, int tContent, int cContenc, int gContent) {
        return (double) Math.round(aContent * 313.21 + tContent * 304.2 + cContenc * 289.18 + gContent * 329.21 * 100.00) / 100.00;
    }

    /**
     * Calculating the GC content
     *
     * @return double - the GC content in %
     */
    public static double gcCalc(int aContent, int tContent, int cContent, int gContent) {
        double sum = aContent + tContent + cContent + gContent;
        double gcSum = cContent + gContent;
        return (gcSum / sum) * 100;
    }

    /**
     * Counting the nearest neighbours
     *
     * @return int - aatt, atta, taat, cagt, ctga, gact, gtca, cggc, gccg, ggcc
     */
    public static int neighbourCounter(char[] forward, char first, char second, char third, char fourth) {
        int nbrs = 0;
        for (int i = 0; i < forward.length - 1; i++) {
            if ((forward[i] == first && forward[i + 1] == second) ||
                    (forward[i] == fourth && forward[i + 1] == third)) {
                nbrs++;
            }
        }
        return nbrs;
    }

    /**
     * Calculating deltaG
     *
     * @return double - deltaG
     */
    public static double countDeltaG(int aatt, int atta, int taat, int cagt, int ctga,
                                     int gact, int gtca, int cggc, int gccg, int ggcc) {
        return (double) Math.round(Math.abs(aatt * -1.2 + atta * -0.9 + taat * -0.9 + cagt * -1.7 + gtca * -1.5 +
                ctga * -1.5 + gact * -1.5 + cggc * -2.8 + gccg * -2.3 + ggcc * -2.1) * 100.00) / 100.00;
    }

    /**
     * Calculating deltaH
     *
     * @return double - deltaH
     */
    public static double countDeltaH(int aatt, int atta, int taat, int cagt, int ctga,
                                     int gact, int gtca, int cggc, int gccg, int ggcc) {
        return (double) Math.round(Math.abs(aatt * -8 + atta * -5.6 + taat * -6.6 + cagt * -8.2 + gtca * -9.4 +
                ctga * -6.6 + gact * -8.8 + cggc * -11.8 + gccg * -10.5 + ggcc * -10.9) * 100.00) / 100.00;
    }

    /**
     * Calculating deltaS
     *
     * @return double - deltaS
     */
    public static double countDeltaS(int aatt, int atta, int taat, int cagt, int ctga,
                                     int gact, int gtca, int cggc, int gccg, int ggcc) {
        return (double) Math.round(Math.abs(aatt * -21.9 + atta * -15.2 + taat * -18.4 + cagt * -21 + gtca * -25.5 +
                ctga * -16.4 + gact * -23.5 + cggc * -29 + gccg * -26.4 + ggcc * -28.4) * 100.00) / 100.00;
    }

    /**
     * Calculating R*ln(K) value
     *
     * @return long - rlnkValue
     */
    public static double rlnkCalc(double primerConc) {
        var k = (1 / (primerConc * Math.pow(10, -9)));               //10^-9: converting nanomols to mols
        double r = 1.987;
        return (double) Math.round((r * Math.log(k) * 100.00)) / 100.00;
    }

    /**
     * Calculating the Tm with the basic method
     *
     * @return double - tmBasic
     */
    public static double tmBasicCalc(int aContent, int tContent, int cContent, int gContent, char[] sequence) {
        var tmBasic = 0.0;
        if (sequence.length < 14) {
            tmBasic = 2 * (aContent + tContent) + 4 * (cContent + gContent);
        } else {
            tmBasic = 64.9 + 41 * ((cContent + gContent - 16.4) / sequence.length);
        }
        return (double) Math.round(tmBasic * 100.00) / 100.00;
    }

    /**
     * Calculating the Tm with the 'salt adjusted' method
     *
     * @return double tmSalty
     */
    public static double tmSaltyCalc(double saltConc, int aContent, int tContent,
                                     int cContent, int gContent, char[] sequence) {
        var tmSalty = 0.0;
        if (sequence.length < 14) {
            tmSalty = 2 * (aContent + tContent) + 4 * (cContent + gContent) - 16.6 * Math.log10(0.05) + 16.6 * Math.log10(saltConc / 1000);
        } else {
            tmSalty = 100.5 + (41 * ((cContent + gContent) / sequence.length) - (820 / sequence.length))
                    + 16.6 * Math.log10(saltConc / 1000);
        }
        return (double) Math.round(tmSalty * 100.00) / 100.00;
    }

    /**
     * Calculating the Tm with the 'nearest neighbour' method
     *
     * @return long - nearest neighbour
     */
    public static double tmNnCalc(double deltaH, double deltaS, double rlnkValue, double saltConc) {
        return (double) Math.round(1000 * ((deltaH - 3.4) / (deltaS + rlnkValue)) - 272.9 + 16.6 * (Math.log10(saltConc / 1000)) * 100.00) / 100.00;
    }
}
