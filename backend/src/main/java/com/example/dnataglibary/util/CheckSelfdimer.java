package com.example.dnataglibary.util;

import com.example.dnataglibary.model.DnaSequence;

public class CheckSelfdimer {

    static final int MINSTEM= 4;

    public static boolean checkSelfDimer(DnaSequence s) {
        var forward = s.getForward().toLowerCase().toCharArray();
        int shifts = 0;
        int pairsinrow = 0;
        int j = forward.length - 1;

        for (int i = 0; i <= forward.length - 1; i++) {
            for (int back = 0; back <= shifts; back++) {
                if ((forward[i - back] == 'a' && forward[j - back] == 't')
                        || (forward[i - back] == 't' && forward[j - back] == 'a')
                        || (forward[i - back] == 'c' && forward[j - back] == 'g')
                        || (forward[i - back] == 'g' && forward[j - back] == 'c')) {
                    pairsinrow++;
                    if (pairsinrow >= MINSTEM) {
                        return true;
                    }
                } else {
                    pairsinrow = 0;
                }
            }
            shifts++;
        }
        return false;
    }
}