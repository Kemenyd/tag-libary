package com.example.dnataglibrary.mapper;


import com.example.dnataglibrary.model.DnaSequence;
import com.example.dnataglibrary.web.dto.SequenceInputRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DnaSequenceMapper {

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "createdAt", ignore = true)
  @Mapping(target = "reverse", ignore = true)
  @Mapping(target = "complementerTtoF", ignore = true)
  @Mapping(target = "complementerFtoT", ignore = true)
  @Mapping(target = "AContent", ignore = true)
  @Mapping(target = "TContent", ignore = true)
  @Mapping(target = "CContent", ignore = true)
  @Mapping(target = "GContent", ignore = true)
  @Mapping(target = "molecularWeight", ignore = true)
  @Mapping(target = "gcContent", ignore = true)
  @Mapping(target = "aatt", ignore = true)
  @Mapping(target = "atta", ignore = true)
  @Mapping(target = "taat", ignore = true)
  @Mapping(target = "cagt", ignore = true)
  @Mapping(target = "ctga", ignore = true)
  @Mapping(target = "gact", ignore = true)
  @Mapping(target = "gtca", ignore = true)
  @Mapping(target = "cggc", ignore = true)
  @Mapping(target = "gccg", ignore = true)
  @Mapping(target = "ggcc", ignore = true)
  @Mapping(target = "deltaG", ignore = true)
  @Mapping(target = "deltaH", ignore = true)
  @Mapping(target = "deltaS", ignore = true)
  @Mapping(target = "rlnkValue", ignore = true)
  @Mapping(target = "tmBasic", ignore = true)
  @Mapping(target = "tmSalty", ignore = true)
  @Mapping(target = "tmNn", ignore = true)
  @Mapping(target = "selfdimer", ignore = true)
  @Mapping(target = "hairloop", ignore = true)
  DnaSequence toEntity(SequenceInputRequest sequenceInputRequest);
}