package com.example.dnataglibrary.web.dto;

import com.example.dnataglibrary.web.validator.Name;
import com.example.dnataglibrary.web.validator.Number;
import com.example.dnataglibrary.web.validator.Sequence;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ProcedureInputRequest {

  @Name
  @NotBlank
  private String editor;
  private String notes;
  @Sequence
  @NotBlank
  private String forward;
  @Number
  @NotBlank
  private double primerConc;
  @Number
  @NotBlank
  private double saltConc;
}
