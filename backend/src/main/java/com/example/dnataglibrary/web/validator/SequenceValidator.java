package com.example.dnataglibrary.web.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SequenceValidator implements ConstraintValidator<Sequence, String> {

  @Override
  public void initialize(Sequence constraintAnnotation) {
    ConstraintValidator.super.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return value != null && value.matches("(^[AaCcGgTt]{10}$)");
  }
}
