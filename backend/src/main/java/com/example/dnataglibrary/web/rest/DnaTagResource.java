package com.example.dnataglibrary.web.rest;

import com.example.dnataglibrary.model.DnaSequence;
import com.example.dnataglibrary.web.dto.SequenceInputRequest;
import com.example.dnataglibrary.service.DnaTagService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/dna-calculator")
@CrossOrigin(origins = "http://localhost:4200")
public class DnaTagResource {

  private final DnaTagService dnaTagService;

  @Autowired
  public DnaTagResource(DnaTagService dnaTagService) {
    this.dnaTagService = dnaTagService;
  }

  @GetMapping("/all")
  public  ResponseEntity<List<DnaSequence>> getAllSequences(){
    var dnaSequences = dnaTagService.getAllSequences();
    return new ResponseEntity<>(dnaSequences, HttpStatus.OK);
  }

  @GetMapping("/find/{id}")
  public ResponseEntity<DnaSequence> getSequenceById (@PathVariable("id") Long id) {
    var dnaSequence = dnaTagService.getSequence(id);
    return new ResponseEntity<>(dnaSequence, HttpStatus.OK);
  }

  @PostMapping("/add")
  public  ResponseEntity<DnaSequence> createSequence(@RequestBody SequenceInputRequest sequenceInputRequest){
    var createDna = dnaTagService.createSequence(sequenceInputRequest);
    return new ResponseEntity<>(createDna, HttpStatus.CREATED);
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<?> deleteSequence(@PathVariable("id") Long id){
    dnaTagService.deleteSequence(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
