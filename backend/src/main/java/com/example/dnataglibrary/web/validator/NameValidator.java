package com.example.dnataglibrary.web.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidator implements ConstraintValidator<Name, String> {

    @Override
    public void initialize(Name constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value != null && value.matches("^([A-ZÁÉÚŐÓÜÖÍ][a-záéúőóüöí]*)+ ([A-ZÁÉÚŐÓÜÖÍ][a-záéúőóüöí]*)$");
    }
}
