package com.example.dnataglibrary.web.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NumberValidator implements ConstraintValidator<Number, String> {

    @Override
    public void initialize(Number constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value != null && value.matches("(^[0-9]{1}|[0-9]{2}|[0-9]{3}|1000)");
    }
}
