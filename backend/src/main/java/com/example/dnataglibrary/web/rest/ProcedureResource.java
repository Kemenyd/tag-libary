package com.example.dnataglibrary.web.rest;

import com.example.dnataglibrary.model.Procedure;
import com.example.dnataglibrary.web.dto.ProcedureInputRequest;
import com.example.dnataglibrary.service.ProcedureService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/procedure")
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class ProcedureResource {

  private final ProcedureService procedureService;

  @GetMapping("/all")
  public ResponseEntity<List<Procedure>> getAllSequences(){
    var procedure = procedureService.getAllProcedure();
    return new ResponseEntity<>(procedure, HttpStatus.OK);
  }

  @GetMapping("/find/{id}")
  public ResponseEntity<Procedure> getSequenceById (@PathVariable("id") Long id) {
    var procedure = procedureService.getProcedure(id);
    return new ResponseEntity(procedure, HttpStatus.OK);
  }

  @PostMapping("/add")
  public  ResponseEntity<Procedure> createSequence(@RequestBody ProcedureInputRequest procedureInputRequest){
    var procedure = procedureService.createProcedure(procedureInputRequest);
    return new ResponseEntity<>(procedure, HttpStatus.CREATED);
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<?> deleteProcedure(@PathVariable("id") Long id){
    procedureService.deleteProcedure(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
