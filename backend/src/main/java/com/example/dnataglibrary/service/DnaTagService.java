package com.example.dnataglibrary.service;

import com.example.dnataglibrary.model.DnaSequence;
import com.example.dnataglibrary.repository.DnaTagRepository;
import com.example.dnataglibrary.util.InitDnaSequences;
import com.example.dnataglibrary.web.dto.SequenceInputRequest;
import com.example.dnataglibrary.web.exception.SequenceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DnaTagService {

    private DnaTagRepository dnaTagRepository;
    private InitDnaSequences initDnaSequences;

    @Autowired
    DnaTagService(DnaTagRepository dnaTagRepository, InitDnaSequences initDnaSequences) {
        this.dnaTagRepository = dnaTagRepository;
        this.initDnaSequences = initDnaSequences;
    }

    public List<DnaSequence> getAllSequences() {
        return dnaTagRepository.findAllByIsGeneratedFalseOrderByCreatedAtDesc();
    }

    public DnaSequence getSequence(Long id) {
        return dnaTagRepository.findById(id).orElseThrow(SequenceNotFoundException::new);
    }

    public DnaSequence createSequence(SequenceInputRequest sequenceInputRequest) {
        var dnaSequence = InitDnaSequences.initDnaSequence(sequenceInputRequest.getForward().toLowerCase(),
                sequenceInputRequest.getPrimerConc(), sequenceInputRequest.getSaltConc(), false);
        return dnaTagRepository.save(dnaSequence);
    }

    public void deleteSequence(Long id) {
        dnaTagRepository.deleteById(id);
    }
}
