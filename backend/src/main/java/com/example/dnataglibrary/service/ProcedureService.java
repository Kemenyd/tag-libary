package com.example.dnataglibrary.service;

import com.example.dnataglibrary.mapper.ProcedureMapper;
import com.example.dnataglibrary.model.DnaSequence;
import com.example.dnataglibrary.model.Procedure;
import com.example.dnataglibrary.repository.ProcedureRepository;
import com.example.dnataglibrary.util.CheckDimer;
import com.example.dnataglibrary.util.InitDnaSequences;
import com.example.dnataglibrary.web.dto.ProcedureInputRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProcedureService {

    public final ProcedureRepository procedureRepository;
    public final ProcedureMapper procedureMapper;
    public final InitDnaSequences initDnaSequences;

    public List<Procedure> getAllProcedure() {
        return procedureRepository.findAll();
    }

    public Optional<Procedure> getProcedure(Long id) {
        return procedureRepository.findById(id);
    }

    public Procedure createProcedure(ProcedureInputRequest procedureInputRequest) {
        var procedure = procedureMapper.toEntity(procedureInputRequest);
        procedure.setCreatedAt(LocalDate.now());
        var dnaSeq = InitDnaSequences.initDnaSequence(procedureInputRequest.getForward().toLowerCase(),
                procedureInputRequest.getPrimerConc(), procedureInputRequest.getSaltConc(), false);
        var highscore = 0;
        DnaSequence genSeq = null;
        boolean molecularWeightCheck = false;
        boolean meltingTemperatureCheck = false;

        while (4 > highscore) {
            genSeq = InitDnaSequences.initDnaSequence(InitDnaSequences.generateSequence(dnaSeq),
                    procedureInputRequest.getPrimerConc(), procedureInputRequest.getSaltConc(), true);
            molecularWeightCheck = molecularWeightDifference(genSeq.getMolecularWeight(), dnaSeq.getMolecularWeight());
            meltingTemperatureCheck = meltingTemperatureCheck(genSeq,dnaSeq);
            if (molecularWeightCheck && meltingTemperatureCheck) {
                highscore = CheckDimer.checkDimer(dnaSeq.getForward(), genSeq.getForward());
            }
        }

        procedure.setScores(highscore);
        procedure.setCreatedAt(LocalDate.now());
        procedure.setDnaSequence(dnaSeq);
        procedure.setGeneratedSequence(genSeq);
        return procedureRepository.save(procedure);
    }

    public void deleteProcedure(Long id) {
        procedureRepository.deleteById(id);
    }

    private boolean molecularWeightDifference(double generatedMolecularWeight, double dnaMolecularWeight) {
        return (Math.abs(generatedMolecularWeight - dnaMolecularWeight) > 0 && Math.abs(generatedMolecularWeight - dnaMolecularWeight) < 150) ||
                (Math.abs(dnaMolecularWeight - generatedMolecularWeight) > 0 && Math.abs(dnaMolecularWeight - generatedMolecularWeight) < 150);
    }

    private boolean meltingTemperatureCheck(DnaSequence generated, DnaSequence sequence){
        return generated.getTmBasic() == sequence.getTmBasic();
    }
}
