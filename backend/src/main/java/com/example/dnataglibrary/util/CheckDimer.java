package com.example.dnataglibrary.util;

public class CheckDimer {
    public static int checkDimer(String dnaSequence53, String dnaSequence35) {

        char[] upperSequence = dnaSequence53.toLowerCase().toCharArray();
        char[] lowerSequence = dnaSequence35.toLowerCase().toCharArray();
        var shift = 0;
        int c = upperSequence.length + lowerSequence.length - 2;
        int i, j, h, g;
        int x, y, z, w;
        int m = 0;
        int score = 0;
        int highestScore = -99;
        for (shift = 0; shift <= c; shift++) {
            if (shift <= c / 2) {
                for (h = 0, g = shift, i = upperSequence.length - 1, j = 0; h <= shift; h++, g--) {
                    if ((upperSequence[i - h] == 'a' && lowerSequence[j + g] == 't') ||
                            (upperSequence[i - h] == 't' && lowerSequence[j + g] == 'a') ||
                            (upperSequence[i - h] == 'g' && lowerSequence[j + g] == 'c') ||
                            (upperSequence[i - h] == 'c' && lowerSequence[j + g] == 'g')) {
                        score++;
                    } else {
                        score--;
                    }
                }
                if (score > highestScore) {
                    highestScore = score;
                }
                score = 0;
                c--;
            } else {
                w = (c / 2) - m - 1;
                for (z = 0, x = 0, y = lowerSequence.length - 1; shift + z <= c; z++, w--) {
                    if ((upperSequence[x + z] == 'a' && lowerSequence[y - w] == 't') ||
                            (upperSequence[x + z] == 't' && lowerSequence[y - w] == 'a') ||
                            (upperSequence[x + z] == 'g' && lowerSequence[y - w] == 'c') ||
                            (upperSequence[x + z] == 'c' && lowerSequence[y - w] == 'g')) {
                        score++;
                    } else {
                        score--;
                    }
                }
                if (score > highestScore) {
                    highestScore = score;
                }
                score = 0;
                m++;
            }
        }
        return highestScore;
    }
}
