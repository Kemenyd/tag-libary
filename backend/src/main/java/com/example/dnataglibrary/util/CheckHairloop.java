package com.example.dnataglibrary.util;

import com.example.dnataglibrary.model.DnaSequence;

public class CheckHairloop {
  static final int MIN_STEM = 3;
  static final int LOOP_SPACE = 5;

  public static boolean checkHairpin (DnaSequence s) {
    var forward = s.getForward().toLowerCase().toCharArray();
    int pairsInRow = 0;

    for (int i=LOOP_SPACE +1; i<=forward.length-1; i++) {
      int until = i%2==0 ?  LOOP_SPACE :  LOOP_SPACE+1;
      for (int back = 0; back < (i- until)/2; back++) {
        if ((forward[i - back] == 'a' && forward[back] == 't')
            || (forward[i - back] == 't' && forward[back] == 'a')
            || (forward[i - back] == 'c' && forward[back] == 'g')
            || (forward[i - back] == 'g' && forward[back] == 'c')) {
          pairsInRow++;
          if (pairsInRow >= MIN_STEM) {
            return true;
          }
        } else {
          pairsInRow = 0;
          }
        }
      }

    int midstart = forward.length%2==0 ? forward.length-LOOP_SPACE : forward.length-1-LOOP_SPACE;
    for ( int j=midstart; j>= LOOP_SPACE + 1; j--) {
      int until = j%2==0 ?  LOOP_SPACE :  LOOP_SPACE+1;
      for (int back = 0; back < (j- until)/2; back++) {
        if ((forward[j - back] == 'a' && forward[back] == 't')
            || (forward[j - back] == 't' && forward[back] == 'a')
            || (forward[j - back] == 'c' && forward[back] == 'g')
            || (forward[j - back] == 'g' && forward[back] == 'c')) {
          pairsInRow++;
          if (pairsInRow >= MIN_STEM) {
            return true;
          }
        } else {
          pairsInRow = 0;
        }
      }
    }
    return false;
  }
}
