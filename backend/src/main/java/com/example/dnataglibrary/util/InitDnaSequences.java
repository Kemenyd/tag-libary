package com.example.dnataglibrary.util;

import com.example.dnataglibrary.model.DnaSequence;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Random;

@Component
public class InitDnaSequences {

    public static DnaSequence initDnaSequence(String sequence, double primerConc, double saltConc, boolean isGenerated) {
        DnaSequence sequenceToSave = new DnaSequence();

        char[] sequenceArray = sequence.toCharArray();
        sequenceToSave.setCreatedAt(LocalDate.now());
        sequenceToSave.setForward(sequence.toUpperCase(Locale.ROOT));
        sequenceToSave.setPrimerConc(primerConc);
        sequenceToSave.setSaltConc(saltConc);
        sequenceToSave.setReverse(CommonUtil.makeReverse(sequence).toUpperCase(Locale.ROOT));
        sequenceToSave.setComplementerTtoF(CommonUtil.makeTtoFComplementer(sequenceArray).toUpperCase(Locale.ROOT));
        sequenceToSave.setComplementerFtoT(
                CommonUtil.makeFtoTComplementer(sequenceToSave.getComplementerTtoF().toUpperCase(Locale.ROOT)));
        sequenceToSave.setAContent(CommonUtil.aContent(sequence));
        sequenceToSave.setTContent(CommonUtil.tContent(sequence));
        sequenceToSave.setCContent(CommonUtil.cContent(sequence));
        sequenceToSave.setGContent(CommonUtil.gContent(sequence));
        sequenceToSave.setMolecularWeight(CommonUtil.mwCalc(
                sequenceToSave.getAContent(),
                sequenceToSave.getTContent(),
                sequenceToSave.getCContent(),
                sequenceToSave.getGContent())
        );
        sequenceToSave.setGcContent(CommonUtil.gcCalc(
                sequenceToSave.getAContent(),
                sequenceToSave.getTContent(),
                sequenceToSave.getCContent(),
                sequenceToSave.getGContent())
        );
        sequenceToSave.setAatt(CommonUtil.neighbourCounter(sequenceArray, 'a', 'a', 't', 't'));
        sequenceToSave.setAtta(CommonUtil.neighbourCounter(sequenceArray, 'a', 't', 't', 'a'));
        sequenceToSave.setTaat(CommonUtil.neighbourCounter(sequenceArray, 't', 'a', 'a', 't'));
        sequenceToSave.setCagt(CommonUtil.neighbourCounter(sequenceArray, 'c', 'a', 'g', 't'));
        sequenceToSave.setCtga(CommonUtil.neighbourCounter(sequenceArray, 'c', 't', 'g', 'a'));
        sequenceToSave.setGact(CommonUtil.neighbourCounter(sequenceArray, 'g', 'a', 'c', 't'));
        sequenceToSave.setGtca(CommonUtil.neighbourCounter(sequenceArray, 'g', 't', 'c', 'a'));
        sequenceToSave.setCggc(CommonUtil.neighbourCounter(sequenceArray, 'c', 'g', 'g', 'c'));
        sequenceToSave.setGccg(CommonUtil.neighbourCounter(sequenceArray, 'g', 'c', 'c', 'g'));
        sequenceToSave.setGgcc(CommonUtil.neighbourCounter(sequenceArray, 'g', 'g', 'c', 'c'));
        sequenceToSave.setDeltaG(
                CommonUtil.countDeltaG(sequenceToSave.getAatt(), sequenceToSave.getAtta(),
                        sequenceToSave.getTaat(), sequenceToSave.getCagt(), sequenceToSave.getCtga(), sequenceToSave.getGact(),
                        sequenceToSave.getGtca(), sequenceToSave.getCggc(), sequenceToSave.getGccg(), sequenceToSave.getGgcc())
        );
        sequenceToSave.setDeltaH(
                CommonUtil.countDeltaH(sequenceToSave.getAatt(), sequenceToSave.getAtta(),
                        sequenceToSave.getTaat(), sequenceToSave.getCagt(), sequenceToSave.getCtga(), sequenceToSave.getGact(),
                        sequenceToSave.getGtca(), sequenceToSave.getCggc(), sequenceToSave.getGccg(), sequenceToSave.getGgcc())
        );
        sequenceToSave.setDeltaS(CommonUtil.countDeltaS(sequenceToSave.getAatt(), sequenceToSave.getAtta(),
                sequenceToSave.getTaat(), sequenceToSave.getCagt(), sequenceToSave.getCtga(), sequenceToSave.getGact(),
                sequenceToSave.getGtca(), sequenceToSave.getCggc(), sequenceToSave.getGccg(), sequenceToSave.getGgcc())
        );
        sequenceToSave.setRlnkValue(CommonUtil.rlnkCalc(primerConc));
        sequenceToSave.setTmBasic(CommonUtil.tmBasicCalc(sequenceToSave.getAContent(),
                sequenceToSave.getTContent(),
                sequenceToSave.getCContent(),
                sequenceToSave.getGContent(),
                sequenceArray)
        );
        sequenceToSave.setTmSalty(CommonUtil.tmSaltyCalc(saltConc,
                sequenceToSave.getAContent(),
                sequenceToSave.getTContent(),
                sequenceToSave.getCContent(),
                sequenceToSave.getGContent(),
                sequenceArray)
        );
        sequenceToSave.setTmNn(CommonUtil.tmNnCalc(sequenceToSave.getDeltaH(),
                sequenceToSave.getDeltaS(),
                sequenceToSave.getRlnkValue(),
                saltConc)

        );
        sequenceToSave.setGenerated(isGenerated);
        sequenceToSave.setSelfdimer(CheckSelfdimer.checkSelfDimer(sequenceToSave));
        sequenceToSave.setHairloop(CheckHairloop.checkHairpin(sequenceToSave));
        return sequenceToSave;
    }

    public static String generateSequence(DnaSequence dnaSequence) {
        Random random = new Random();
        char[] array = {'a', 'c', 'g', 't'};
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < dnaSequence.getForward().length(); i++) {
            var y = random.nextInt(4);
            sb.append(array[y]);
        }
        return sb.toString();
    }
}

