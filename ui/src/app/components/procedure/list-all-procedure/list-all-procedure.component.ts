import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProcedureService} from "../../../service/procedure/procedure.service";
import {ProcedureResponse} from "../../shared/ProcedureResponse";
import {ProcedureCreateRequest} from "../../shared/ProcedureCreateRequest";
import {Constants} from "../../shared/Constants";

@Component({
  selector: 'app-list-all-procedure',
  templateUrl: './list-all-procedure.component.html',
  styleUrls: ['./list-all-procedure.component.css']
})
export class ListAllProcedureComponent implements OnInit {
  procedures: Array<ProcedureResponse> | undefined;
  addProcedureForm: FormGroup = new FormGroup({});
  procedureCreateRequest: ProcedureCreateRequest = new ProcedureCreateRequest();

  constructor(private procedureService: ProcedureService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.addProcedureForm = this.formBuilder.group({
      editor: ['',[
        Validators.required,
        Validators.pattern(Constants.NAME_PATTERN)
      ]],
      sequence: ['',[
        Validators.required,
        Validators.pattern(Constants.SEQUENCE_PATTERN)
      ]],
      primer: ['',[Validators.required,
        Validators.pattern(Constants.NUMBER_PATTERN)
      ]],
      salt: ['',[Validators.required,
        Validators.pattern(Constants.NUMBER_PATTERN)
      ]],
      notes: ['',[]]
    });
    this.procedureService.getProcedures().subscribe(data=>{
      this.procedures = data;
    })
  }

  navigate(id: number) {
    this.router.navigate(['procedures/view/', id])
  }

  createProcedure() {
    if (this.addProcedureForm.valid) {
      const reqBody: ProcedureCreateRequest = {
        editor: this.addProcedureForm.get('editor')?.value,
        notes: this.addProcedureForm.get('notes')?.value,
        forward: this.addProcedureForm.get('sequence')?.value,
        primerConc: this.addProcedureForm.get('primer')?.value,
        saltConc: this.addProcedureForm.get('salt')?.value
      };
      this.procedureService.createProcedure(reqBody).subscribe(
        () => {
          window.location.reload();
        });
    }
  }

  delete(id: number){
    this.procedureService.deleteProcedure(id).subscribe((data: any)=> {
      if (data === null){
        window.location.reload();
      }
    });
  }
}
