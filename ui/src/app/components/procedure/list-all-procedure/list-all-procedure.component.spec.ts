import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAllProcedureComponent } from './list-all-procedure.component';

describe('ListAllProcedureComponent', () => {
  let component: ListAllProcedureComponent;
  let fixture: ComponentFixture<ListAllProcedureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAllProcedureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAllProcedureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
