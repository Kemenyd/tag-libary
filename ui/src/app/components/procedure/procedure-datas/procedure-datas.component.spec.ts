import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureDatasComponent } from './procedure-datas.component';

describe('ProcedureDatasComponent', () => {
  let component: ProcedureDatasComponent;
  let fixture: ComponentFixture<ProcedureDatasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcedureDatasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureDatasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
