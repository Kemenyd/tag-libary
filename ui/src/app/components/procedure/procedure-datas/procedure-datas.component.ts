import { Component, OnInit } from '@angular/core';
import {ProcedureResponse} from "../../shared/ProcedureResponse";
import {ActivatedRoute} from "@angular/router";
import {ProcedureService} from "../../../service/procedure/procedure.service";
import { SequenceResponse } from '../../shared/SequenceResponse';

@Component({
  selector: 'app-procedure-datas',
  templateUrl: './procedure-datas.component.html',
  styleUrls: ['./procedure-datas.component.css']
})
export class ProcedureDatasComponent implements OnInit {

  procedureId!: any;
  procedure: ProcedureResponse = new ProcedureResponse();
  dnaSequence: SequenceResponse = new SequenceResponse();
  generatedSequence: SequenceResponse = new SequenceResponse();
  constructor(private activedRoute: ActivatedRoute,private procedureService: ProcedureService) { }

  ngOnInit(): void {
    this.activedRoute.params.subscribe(params=>{
      this.procedureId = params['id'];
    })
    this.procedureService.findProcedures(this.procedureId).subscribe(data=>{
      this.procedure = data;
      if(data != null){
        this.dnaSequence = data.dnaSequence;
        this.generatedSequence = data.generatedSequence;
      }
    })
    console.log(this.procedure);
  }

}
