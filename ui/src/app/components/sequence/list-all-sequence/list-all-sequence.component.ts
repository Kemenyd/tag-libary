import {Component, OnInit} from '@angular/core';
import {SequenceService} from "../../../service/sequence/sequence.service";
import {SequenceResponse} from "../../shared/SequenceResponse";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SequenceCreateRequest} from "../../shared/SequenceCreateRequest";
import {Constants} from "../../shared/Constants";

@Component({
  selector: 'app-list-all-sequence',
  templateUrl: './list-all-sequence.component.html',
  styleUrls: ['./list-all-sequence.component.css']
})
export class ListAllSequenceComponent implements OnInit {

  dnaSequences: Array<SequenceResponse> | undefined;
  addSeqForm: FormGroup = new FormGroup({});
  dnaSeqCreatRequest: SequenceCreateRequest = new SequenceCreateRequest();

  constructor(private sequenceService: SequenceService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.addSeqForm = this.formBuilder.group({
      sequence: ['',[
        Validators.required,
        Validators.pattern(Constants.SEQUENCE_PATTERN)
      ]],
      primer: ['',[Validators.required,
      Validators.pattern(Constants.NUMBER_PATTERN)
      ]],
      salt: ['',[Validators.required,
      Validators.pattern(Constants.NUMBER_PATTERN)
      ]]
    });
    this.sequenceService.getSequences().subscribe(data=>{
      this.dnaSequences = data;
    })
  }

  navigate(id: number) {
    this.router.navigate(['sequence/view/', id])
  }

  createSequence() {
    if (this.addSeqForm.valid) {
      const reqBody: SequenceCreateRequest = {
        forward: this.addSeqForm.get('sequence')?.value,
        primerConc: this.addSeqForm.get('primer')?.value,
        saltConc: this.addSeqForm.get('salt')?.value
      };
      this.sequenceService.createSequence(reqBody).subscribe(
        () => {
          window.location.reload();
        });
    }
  }

  delete(id: number){
    this.sequenceService.deleteSequence(id).subscribe((data: any)=> {
      console.log(data)
      if (data === null){
        window.location.reload();
      }
        });
  }
}
