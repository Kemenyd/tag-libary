import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAllSequenceComponent } from './list-all-sequence.component';

describe('ListAllSequenceComponent', () => {
  let component: ListAllSequenceComponent;
  let fixture: ComponentFixture<ListAllSequenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAllSequenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAllSequenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
