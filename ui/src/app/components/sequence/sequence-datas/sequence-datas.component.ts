import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SequenceService} from "../../../service/sequence/sequence.service";
import {SequenceResponse} from "../../shared/SequenceResponse";

@Component({
  selector: 'app-sequence-datas',
  templateUrl: './sequence-datas.component.html',
  styleUrls: ['./sequence-datas.component.css']
})
export class SequenceDatasComponent implements OnInit {
  dnaId?:any;
  dnaSequence:SequenceResponse = new SequenceResponse();
  constructor(private activedRoute: ActivatedRoute,private sequenceService: SequenceService) { }

  ngOnInit(): void {
    this.activedRoute.params.subscribe(data=> {
      this.dnaId = data['id'];
    });
    this.sequenceService.findSequence(this.dnaId).subscribe(data=>{
      this.dnaSequence = data;
    })
  }

}
