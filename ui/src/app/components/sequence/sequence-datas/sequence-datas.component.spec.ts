import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SequenceDatasComponent } from './sequence-datas.component';

describe('SequenceDatasComponent', () => {
  let component: SequenceDatasComponent;
  let fixture: ComponentFixture<SequenceDatasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SequenceDatasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SequenceDatasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
