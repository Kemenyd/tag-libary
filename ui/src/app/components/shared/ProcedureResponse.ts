import {SequenceResponse} from "./SequenceResponse";

export class ProcedureResponse {
  id!:number;
  createdAt?:Date;
  editor?:string;
  notes?:string;
  dnaSequence!: SequenceResponse;
  generatedSequence!:SequenceResponse;
  scores!:string;
}
