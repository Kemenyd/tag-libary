export class Constants{

  static SEQUENCE_PATTERN = '(^[AaCcGgTt]{10}$)';
  static NUMBER_PATTERN = '(^[0-9]{1}|[0-9]{2}|[0-9]{3}|1000)';
  static NAME_PATTERN = '^([A-ZÁÉÚŐÓÜÖÍ][a-záéúőóüöí]*)+ ([A-ZÁÉÚŐÓÜÖÍ][a-záéúőóüöí]*)$'
}
