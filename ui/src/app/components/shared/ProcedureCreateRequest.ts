export class ProcedureCreateRequest {
  editor!:string;
  notes!:string;
  forward!:string;
  primerConc!:number;
  saltConc!:number;
}
