import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ListAllProcedureComponent} from "./components/procedure/list-all-procedure/list-all-procedure.component";
import {ProcedureDatasComponent} from "./components/procedure/procedure-datas/procedure-datas.component";
import {SequenceDatasComponent} from "./components/sequence/sequence-datas/sequence-datas.component";
import {ListAllSequenceComponent} from "./components/sequence/list-all-sequence/list-all-sequence.component";
import { LandingPageComponent } from "./components/landingpage/landing-page/landing-page.component";

const routes: Routes = [
  { path: 'sequences', component:  ListAllSequenceComponent},
  { path: 'sequence/view/:id', component:  SequenceDatasComponent},
  { path: 'procedures', component:  ListAllProcedureComponent},
  { path: 'procedures/view/:id', component:  ProcedureDatasComponent},
  { path: 'homepage', component: LandingPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
