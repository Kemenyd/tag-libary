import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule} from "@angular/common/http";
import { AppRoutingModule} from "./app.routing";
import { ListAllSequenceComponent } from './components/sequence/list-all-sequence/list-all-sequence.component';
import { SequenceDatasComponent } from './components/sequence/sequence-datas/sequence-datas.component';
import { ListAllProcedureComponent } from './components/procedure/list-all-procedure/list-all-procedure.component';
import { ProcedureDatasComponent } from './components/procedure/procedure-datas/procedure-datas.component';
import { SequenceService} from "./service/sequence/sequence.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule} from "@angular/material/input";
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { MatButtonModule} from "@angular/material/button";
import { MatCardModule} from "@angular/material/card";
import { MatTabsModule} from "@angular/material/tabs";
import { ProcedureService} from "./service/procedure/procedure.service";
import { LandingPageComponent } from './components/landingpage/landing-page/landing-page.component';

@NgModule({
  declarations: [
    AppComponent,
    ListAllSequenceComponent,
    SequenceDatasComponent,
    ListAllProcedureComponent,
    ProcedureDatasComponent,
    LandingPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatCardModule,
    MatTabsModule
  ],
  providers: [SequenceService,ProcedureService],
  bootstrap: [AppComponent]
})
export class AppModule { }
