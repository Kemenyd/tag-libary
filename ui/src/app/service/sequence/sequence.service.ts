import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {SequenceResponse} from "../../components/shared/SequenceResponse";
import {HttpClient} from "@angular/common/http";
import {SequenceCreateRequest} from "../../components/shared/SequenceCreateRequest";

@Injectable({
  providedIn: 'root'
})
export class SequenceService {
  url ="http://localhost:8080/api/dna-calculator";
  constructor(private http: HttpClient) { }

  getSequences(): Observable<Array<SequenceResponse>>{
    return this.http.get<Array<SequenceResponse>>(this.url+"/all")
  }

  findSequence(id: any): Observable<SequenceResponse>{
    return this.http.get<SequenceResponse>(this.url+"/find/"+id)
  }

  createSequence(dnaSequence: SequenceCreateRequest){
    return this.http.post(this.url+"/add", dnaSequence);
  }

  deleteSequence(id: any):any{
    return this.http.delete(this.url+"/delete/"+id);
  }
}
