import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ProcedureCreateRequest} from "../../components/shared/ProcedureCreateRequest";
import {ProcedureResponse} from "../../components/shared/ProcedureResponse";

@Injectable({
  providedIn: 'root'
})
export class ProcedureService {
  url ="http://localhost:8080/api/procedure";
  constructor(private http: HttpClient) { }

  getProcedures(): Observable<Array<ProcedureResponse>>{
    return this.http.get<Array<ProcedureResponse>>(this.url+"/all")
  }

  findProcedures(id: any): Observable<ProcedureResponse>{
    return this.http.get<ProcedureResponse>(this.url+"/find/"+id)
  }

  createProcedure(procedure: ProcedureCreateRequest){
    return this.http.post(this.url+"/add", procedure);
  }

  deleteProcedure(id: any):any{
    return this.http.delete(this.url+"/delete/"+id);
  }
}
